import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Switch, NavLink as RRNavLink } from "react-router-dom"
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';

import './App.css'

import Home from './Home'
import AssignmentOne from "./AssignmentOne";
import AssignmentTwo from "./AssignmentTwo";
import AssignmentThree from "./AssignmentThree";

const App = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <Router>
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">DBT230-S2</NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  PostgreSQL
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    <NavLink tag={RRNavLink} to="/assignmentOne" className="App-navlink">Assignment 1</NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink tag={RRNavLink} to="/assignmentTwo" className="App-navlink">Assignment 2</NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink tag={RRNavLink} to="/assignmentThree" className="App-navlink">Assignment 3</NavLink>
                  </DropdownItem>
                  <DropdownItem divider />
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </div>

      <Switch>
        <Route path="/assignmentOne">
          <AssignmentOne />
        </Route>
        <Route path="/assignmentTwo">
          <AssignmentTwo />
        </Route>
        <Route path="/assignmentThree">
          <AssignmentThree />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;