import React, { useEffect, useState } from "react";

function Home() {
    const [message, setMessage] = useState('Backend is loading...')

    /**
     * Sanity check that we are connected to the backend
     */
    useEffect(() => {
        setTimeout(function() { //Start the timer
            console.log(1)
            fetch('http://localhost:8055/api', {
                'method': 'GET',
                'content-type': 'application/json',
                'accept': 'application/json'
            })
                .then(function(response){ return response.json(); })
                .then(function(data) {
                    setMessage(data['message'])
                })
        }, 1000)
    })

    return (
        <div>
            <p>
                This is the home page, message: {message}
            </p>
        </div>
    );
}

export default Home;
