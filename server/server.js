const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()

const corsOptions = {
    origin: 'http://localhost:3000'
}

app.use(cors(corsOptions))
app.use(bodyParser.urlencoded({ extended: true }))

/* https://mherman.org/blog/designing-a-restful-api-with-node-and-postgres/ */
const promise = require('bluebird');
const options = {
    // Initialization Options
    promiseLib: promise
};

const pgp = require('pg-promise')(options);
const connectionString = 'postgres://dstephenson:test@localhost:5432/7dbs';
const db = pgp(connectionString);

/*
* Places your RESTful CRUD calls here
 */
app.get('/api', (req, res) => {
    db.any('SELECT NOW()')
        .then(function(data) {
            console.log(data)
        })
        .catch(function(err) {
            console.log(err)
        })

    res.json({ message: 'Successfully hit your Node backend! '})
})

const PORT = process.env.PORT || 8055
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`)
})